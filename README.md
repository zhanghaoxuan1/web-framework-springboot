# 软件开发与管理 - springboot

#### 介绍
本仓库用于软件开发与管理课程，项目采用springboot技术

使用springjdbc进行持久化，实现登陆功能

使用springboot进行单元测试功能

测试yaml配置，测试@Value读取yaml配置信息

测试springboot整合hibernate的操作

测试springboot整合mybatis的操作

测试thymeleaf的功能

测试过滤器的功能，作用在一次请求中

测试拦截器的功能，作用在方法前后，对方法进行拦截，相当于aop的功能

测试监听器的功能，实现统计在线登陆人数功能，和在线人数功能