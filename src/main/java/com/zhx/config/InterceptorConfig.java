package com.zhx.config;

import com.zhx.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 22:04
 **/
//@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //需要拦截的路径 /**表示所有的请求都需要拦截
        String[] addPathPatterns={
               "/**"
        };
        //不需要拦截的路径
        String[] excludePathPatterns={
                "/user/",
                "/user/index.html",
                "/user/loginCheck.html"
        };
        //注册一个登陆拦截器
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns(addPathPatterns)
                .excludePathPatterns(excludePathPatterns);
    }
}
