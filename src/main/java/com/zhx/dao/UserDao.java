package com.zhx.dao;

import com.zhx.domain.User;

public interface UserDao {
    int getMatchCount(String username,String password);
    User findUserbyName(String username);
}
