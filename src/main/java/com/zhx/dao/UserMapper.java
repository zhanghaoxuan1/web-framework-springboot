package com.zhx.dao;

import com.zhx.domain.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 18:06
 **/
public interface UserMapper {
    int getMatchCount(@Param("username") String username, @Param("password") String password);
    User findUserbyName(@Param("username") String username);
    //插入用户
    void insertUser(User user);
}
