package com.zhx.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 16:44
 **/
public class BaseDao<T> { //1.提供DAO类级别的的泛型支持
    @Autowired
    private HibernateTemplate hibernateTemplate;  //2.注入Hibernate的模板类

    private Class entityClass; //3.DAO的泛型类，也就是子类所指定的T对应的类型

    public BaseDao(){
        Type type = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) type).getActualTypeArguments();
        entityClass= (Class) params[0];
    }
    public T get(Serializable id){
        return (T) hibernateTemplate.get(entityClass, id); //直接使用entityClass
    }
    public void save(){
        hibernateTemplate.save(entityClass);
    }
    public void update(){
        hibernateTemplate.update(entityClass);
    }
    public void delete(){
        hibernateTemplate.delete(entityClass);
    }
    public HibernateTemplate getHibernateTemplate() {
        return hibernateTemplate;
    }
}
