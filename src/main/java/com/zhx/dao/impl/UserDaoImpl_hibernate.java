package com.zhx.dao.impl;

import com.zhx.dao.UserDao;
import com.zhx.domain.User;

import java.util.List;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 16:47
 **/
public class UserDaoImpl_hibernate extends BaseDao<User> implements UserDao {

    @Override
    public int getMatchCount(String username, String password) {
        Object count = getHibernateTemplate().iterate("select count(u.id) from com.zhx.domain.User u").next();
        return (int) count;
    }

    @Override
    public User findUserbyName(String username) {
        List<User> users = (List<User>) getHibernateTemplate().find("from com.zhx.domain.User u where u.username=?0",username);
        if (users==null){
            return null;
        }else{
            return users.get(0);
        }
    }
}
