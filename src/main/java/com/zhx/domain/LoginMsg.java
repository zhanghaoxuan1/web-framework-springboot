package com.zhx.domain;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-23 23:22
 **/
public class LoginMsg {
    private String title;
    private String error;
    private int onlinecount;

    public LoginMsg() {
    }

    public LoginMsg(String title, String error, int onlinecount) {
        this.title = title;
        this.error = error;
        this.onlinecount = onlinecount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getOnlinecount() {
        return onlinecount;
    }

    public void setOnlinecount(int onlinecount) {
        this.onlinecount = onlinecount;
    }
}
