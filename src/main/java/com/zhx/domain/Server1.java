package com.zhx.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 16:13
 **/
@Component
public class Server1 {
    @Value("${server.port}")
    private int port;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "Server1{" +
                "port=" + port +
                '}';
    }
}
