package com.zhx.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 21:59
 **/
public class LoginInterceptor implements HandlerInterceptor {
    String basePath;
    @Override//进入controller之前执行的方法
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        //登陆请求拦截器
        System.out.println("-登陆请求拦截器-");
        System.out.println(request.getRequestURI().toString());
        //获取当前请求的路径
        basePath = request.getScheme() + "://" + request.getServerName() + ":"+request.getServerPort()+request.getContextPath();
        Object user = request.getSession().getAttribute("user");
        if (user == null){
            System.out.println("用户没有登陆");
//            response.sendRedirect(basePath + "/user/index.html");
            return false;
        }
        //继续提交请求，false，请求不提交
        return true;
    }

}
