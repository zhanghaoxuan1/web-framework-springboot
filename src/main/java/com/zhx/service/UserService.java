package com.zhx.service;

import com.sun.net.httpserver.HttpsServer;
import com.zhx.domain.LoginMsg;
import com.zhx.domain.User;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    int getMatchCount(String username,String password);
    User findUserbyName(String username);
    void setLoginPageModel(Model model , String errorMsg, HttpServletRequest request);
    void setLoginSuccessPageModel(Model model , User user, HttpServletRequest request);
    void quitSystem(Model model, String errorMsg, HttpServletRequest request);

    boolean registerUser(User user);

    void setRegisterPageModel(Model model, String s);
}
