package com.zhx.service.impl;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.zhx.dao.UserDao;
import com.zhx.dao.UserMapper;
import com.zhx.dao.impl.UserDaoImpl;
import com.zhx.domain.LoginMsg;
import com.zhx.domain.User;
import com.zhx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-22 00:38
 **/
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    /**
     * 获取配置的用户个数
     * @param username
     * @param password
     * @return int
     */
    @Override
    public int getMatchCount(String username,String password){
        int matchCount = userMapper.getMatchCount(username, password);
        return matchCount;
    }

    /**
     * 根据用户名查找用户
     * @param username
     * @return User
     */
    @Override
    public User findUserbyName(String username){
        User userbyName = userMapper.findUserbyName(username);
        if (userbyName == null){
            return null;
        }
        return userbyName;
    }

    /**
     * 返回用户的登陆时的信息
     * @param model
     * @param errorMsg
     */
    @Override
    public void setLoginPageModel(Model model, String errorMsg,HttpServletRequest request) {
        int onlineCount = getOnlineCount(request);
        LoginMsg loginMsg = new LoginMsg("登陆页面", errorMsg, onlineCount);
        model.addAttribute("title", loginMsg.getTitle());
        model.addAttribute("error", loginMsg.getError());
        model.addAttribute("onlinecount", loginMsg.getOnlinecount());
    }

    /**
     * 返回登陆成功页面的信息
     * @param model
     * @param user
     * @param request
     */
    @Override
    public void setLoginSuccessPageModel(Model model, User user, HttpServletRequest request) {
        model.addAttribute("user",user );
        int onlineLoginCount = getOnlineLoginCount(request, user);
        model.addAttribute("onlinelogincount", onlineLoginCount);
    }

    /**
     * 退出系统，需要把online中的username变为游客
     * @param model
     * @param errorMsg
     * @param request
     */
    @Override
    public void quitSystem(Model model, String errorMsg, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String id = session.getId();
        ServletContext servletContext = session.getServletContext();
        Map<String,String> online = (Map<String, String>) servletContext.getAttribute("online");
        if (online != null){
            online.put(id, "游客");
        }
        servletContext.setAttribute("online",online );
        setLoginPageModel(model, errorMsg, request);
    }


    /**
     * 进行注册
     * @param user
     * @return
     */
    @Override
    public boolean registerUser(User user) {
        if (user == null){
            return false;
        }
        String username = user.getUsername();
        String password = user.getPassword();
        //判断用户名和密码是否为空
        if (username!=null&&!username.equals("")&&password!=null&&!password.equals("")){
            //如果不为空,进行注册
            try {
                userMapper.insertUser(user);
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }else{
            //用户名或密码为空
            return false;
        }
    }

    /**
     * 返回注册页面的信息封装
     * @param model
     * @param s
     */
    @Override
    public void setRegisterPageModel(Model model, String s) {
        model.addAttribute("error",s);
    }

    //返回在线人数
    public int getOnlineCount(HttpServletRequest request){
        int count = 0 ;
        HttpSession session = request.getSession();
        ServletContext servletContext = session.getServletContext();
        Map<String,String> online = (Map<String, String>) servletContext.getAttribute("online");
        count = online.size();
        return count;
    }
    //返回在线登陆人数
    public int getOnlineLoginCount(HttpServletRequest request,User user){
        int count = 0;
        HttpSession session = request.getSession();
        session.setAttribute("user",user );
        ServletContext servletContext = session.getServletContext();
        Map<String,String> online = (Map<String, String>) servletContext.getAttribute("online");
        String id = session.getId();
        if (online!=null){
            online.put(id, user.getUsername());
        }
        for (Map.Entry<String,String> entry : online.entrySet()){
            if (!entry.getValue().equals("游客")) {
                count++;
            }
        }
        return count;
    }
}
