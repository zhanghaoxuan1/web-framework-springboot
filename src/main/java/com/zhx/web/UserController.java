package com.zhx.web;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.zhx.domain.LoginMsg;
import com.zhx.domain.User;
import com.zhx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sun.rmi.runtime.Log;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @program: web-springboot
 * @author: zhx
 * @create: 2021-05-20 23:41
 **/
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userServiceImpl;

    @RequestMapping(value = {"/","/index.html"},method = {RequestMethod.GET,RequestMethod.POST})
    public String toLoginPage(Model model,HttpServletRequest request){
        userServiceImpl.setLoginPageModel(model,"" ,request );
        return "users/login"; //会去resources下的templates文件夹下的users文件夹下的login.html
    }

    @RequestMapping(value = "/loginCheck.html",method = {RequestMethod.GET,RequestMethod.POST})
    public String login(Model model, User user,HttpServletRequest request){
        System.out.println(user.toString());
        if (user!=null && user.getUsername()!=null && user.getPassword()!=null){
            if (!user.getUsername().equals("")&&!user.getPassword().equals("")){
                int matchCount = userServiceImpl.getMatchCount(user.getUsername(), user.getPassword());
                if (matchCount==0){
                    userServiceImpl.setLoginPageModel(model,"用户名或密码错误" ,request );
                    return "/users/login";
                }
            }else{
                userServiceImpl.setLoginPageModel(model,"用户名或密码为空" ,request );
                return "/users/login";
            }
        }else{
            userServiceImpl.setLoginPageModel(model,"用户名或密码为空" ,request );
            return "/users/login";
        }
        User userbyName = userServiceImpl.findUserbyName(user.getUsername());
        if (userbyName==null){
            userServiceImpl.setLoginPageModel(model,"登陆出错" ,request );
            return "/users/login";
        }
        userServiceImpl.setLoginSuccessPageModel(model, user, request);
        return "/users/loginSuccess";
    }


    @RequestMapping(value = "/toLoginSuccessPage.html")
    public String loginSucess(Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (user!=null){
            userServiceImpl.setLoginSuccessPageModel(model, user, request);
            return "/users/loginSuccess";
        }else{
            userServiceImpl.setLoginPageModel(model,"登陆出错" ,request );
            return "/users/login";
        }
    }

    @RequestMapping(value = "/quit.html") //退出系统
    public String quitSystem(Model model,HttpServletRequest request){
        //销毁 session
        userServiceImpl.quitSystem(model,"" ,request );
        return "/users/login";
    }

    @RequestMapping(value = "/toRegister.html")
    public String toRegisterPage(Model model){
        return "/users/register";
    }


    /**
     * 注册用户
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/registerUser.html")
    public String registerUser(Model model , User user,  HttpServletRequest request){
        boolean isRegister = userServiceImpl.registerUser(user);
        if (isRegister){
            //注册成功
            return "/users/login";
        }else{
            userServiceImpl.setRegisterPageModel(model,"注册失败，重新注册！");
            //注册失败
            return "/users/register";
        }
    }
}
