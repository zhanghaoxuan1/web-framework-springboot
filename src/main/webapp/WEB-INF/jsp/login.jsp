<%@page pageEncoding="UTF-8" language="java" contentType="text/html; UTF-8" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
<h2 align="center">欢迎登陆系统</h2>
<div align="center">
<form action="/springboot/user/loginCheck.html" method="post">
    <table border="2" align="center" cellspacing="0" cellpadding="0" frame="void" rules="none">
        <tr>
            <td align="right">用户名：</td>
            <td align="left"><input type="text" name="username"></td>
        </tr>
        <tr>
            <td align="right">密码：</td>
            <td align="left"><input type="password" name="password"></td>
        </tr>
    </table>
    <p align="center">
        <input type="submit" value="登陆">
        <input type="reset" value="重置">
        <%--<a href="${pageContext.request.contextPath}/user/toRegisterUser.html">注册</a>--%>
    </p>
</form>
</div>

<c:if test="${!empty error}">
    <p align="center">
        <font color="red">
            <c:out value="${error}"/>
        </font>
    </p>
</c:if>
</body>
</html>