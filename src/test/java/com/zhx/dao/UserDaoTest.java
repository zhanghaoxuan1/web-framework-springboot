package com.zhx.dao;

import com.zhx.dao.impl.UserDaoImpl_hibernate;
import com.zhx.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserDaoTest {
    @Autowired
    UserDao userDaoImpl_hibernate;
    @Test
    void getMatchCount() {
        int count = userDaoImpl_hibernate.getMatchCount("zhx", "123");
        System.out.println(count);
    }

    @Test
    void findUserbyName() {
        User zhx = userDaoImpl_hibernate.findUserbyName("zhx");
        System.out.println(zhx);
    }
}