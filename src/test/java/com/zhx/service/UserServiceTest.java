package com.zhx.service;

import com.zhx.dao.UserMapper;
import com.zhx.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;
    @Resource
    private UserMapper userMapper;

    @Test
    void getMatchCount() {
//        int count = userService.getMatchCount("zhx", "123");
        int count = userMapper.getMatchCount("zhx", "123");
        assertEquals(1,count );
    }

    @Test
    void findUserbyName() {
        User zhx = userMapper.findUserbyName("zhx");
        System.out.println(zhx);
    }


}